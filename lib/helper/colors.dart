import 'package:flutter/material.dart';

class CustomColors {
  static const white =
      const Color(0xffFFFFFF); // Second `const` is optional in assignments.
  static const lightGreen =
      const Color(0xff97AB47); // Second `const` is optional in assignments.
  static const darkGreen =
      const Color(0xff5B8B0F); // Second `const` is optional in assignments.
  static const green =
      const Color(0xff0F7A4F); // Second `const` is optional in assignments.
  static const white1 =
      const Color(0xffF4F4F4); // Second `const` is optional in assignments.
  static const grey =
      const Color(0xff716F6F); // Second `const` is optional in assignments.
  static const lightGrey =
      const Color(0xffC5C5C5); // Second `const` is optional in assignments.
  static const vLightGreen =
      const Color(0xffE5FFBC); // Second `const` is optional in assignments.
  static const lightWhite =
      const Color(0xffF7F7F7); // Second `const` is optional in assignments.
  static const green1 =
      const Color(0xff4E790B); // Second `const` is optional in assignments.
  // static const lightWhite =
  //     const Color(0xff97AB47); // Second `const` is optional in assignments.

}
