import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:harabharapakistan/screens/signIn_screen.dart';
import 'package:harabharapakistan/screens/signup_screen.dart';

import 'dashboard_screen.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState

    Future.delayed(Duration(seconds: 3), () {
      Navigator.of(context).pushNamed(DashboardScreen().routeName);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.amberAccent,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            child: Image.asset(
              "assets/bg.png",
              fit: BoxFit.cover,
              // allowDrawingOutsideViewBox: false,
              // width: MediaQuery.of(context).size.width,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: SvgPicture.asset(
              "assets/logo.svg",
              fit: BoxFit.fitWidth,
            ),
          ),
        ],
      ),
    );
  }
}
