import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:harabharapakistan/helper/clipPathClass.dart';
import 'package:harabharapakistan/helper/colors.dart';

class DashboardScreen extends StatefulWidget {
  // const DashboardScreen({Key key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();

  String routeName = "/dashboardScreen";
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            buildTopHalf(context),
            buildMiddleRow(size),
            buildBottomHalfCard(size),
            Spacer(),
            buildButton(
                icon: "tree.svg",
                onPressed: () {},
                text: "Report a Tree",
                color: CustomColors.lightGreen),
            SizedBox(
              height: 10,
            ),
            buildButton(
                icon: "plant.svg",
                onPressed: () {},
                text: "New Plantation",
                color: CustomColors.green1),
            Spacer(),
          ],
        ),
      ),
    );
  }

  SizedBox buildButton(
      {String icon, String text, Function onPressed, Color color}) {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            textColor: Colors.white,
            color: color,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset("assets/$icon"),
                SizedBox(
                  width: 20,
                ),
                Text(
                  text,
                  style: TextStyle(fontFamily: "Futura", fontSize: 18),
                ),
              ],
            ),
            onPressed: onPressed),
      ),
    );
  }

  Widget buildBottomHalfCard(Size size) {
    return Container(
      height: size.height / 5,
      margin: EdgeInsets.all(10).copyWith(top: 20),
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: size.height / 5,
            decoration: BoxDecoration(
              color: CustomColors.lightWhite,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              // boxShadow: [
              //   BoxShadow(
              //     color: Colors.grey.withOpacity(0.5),
              //     spreadRadius: 5,
              //     blurRadius: 7,
              //     offset: Offset(0, 3), // changes position of shadow
              //   ),
              // ],
            ),
            padding: EdgeInsets.all(20).copyWith(right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Go Green Pakistan",
                  style: TextStyle(
                      fontFamily: "JosefinSans",
                      fontSize: 16,
                      color: CustomColors.green),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Expanded(
                      // flex: 2,
                      child: Text(
                        "We need to Plant trees to make our country healthy, clean and green.",
                        style: TextStyle(
                          fontFamily: "Futura",
                          fontSize: 12,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: size.width / 2.5,
                    )
                    // Spacer(),
                  ],
                ),
                // Spacer(),
                // SizedBox(
                //   height: size.height * 0.03,
                // ),
                Spacer(),
                Text(
                  "See stats Here >",
                  style: TextStyle(
                      fontFamily: "JosefinSans",
                      fontSize: 13,
                      color: CustomColors.lightGreen),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Image.asset(
              "assets/handWatering.png",
              fit: BoxFit.fill,
              height: size.height / 5,
              width: size.width / 1.8,
            ),
          ),
        ],
      ),
    );
  }

  Row buildMiddleRow(Size size) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: size.width / 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "12",
                style: TextStyle(
                  fontFamily: "JosefinSans",
                  fontSize: 26,
                  color: CustomColors.green,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Trees Reported",
                style: TextStyle(
                  fontFamily: "JosefinSans",
                  fontSize: 14,
                  color: CustomColors.lightGrey,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 20,
          // width: size.width * 0.1,
        ),
        Container(
          width: 0.5,
          height: 80,
          // height: double.maxFinite,
          color: CustomColors.darkGreen,
        ),
        SizedBox(
          // width: size.width * 0.1,
          width: 20,
        ),
        Container(
          width: size.width / 3,
          child: Column(
            children: [
              Text(
                "12",
                style: TextStyle(
                  fontFamily: "JosefinSans",
                  fontSize: 26,
                  color: CustomColors.green,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "My plants",
                style: TextStyle(
                  fontFamily: "JosefinSans",
                  fontSize: 14,
                  color: CustomColors.lightGrey,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Container buildTopHalf(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2.25,
      color: Colors.white,
      child: Stack(
        children: [
          Positioned(
            height: MediaQuery.of(context).size.height / 2.8,
            // width: double.infinity,
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              height: MediaQuery.of(context).size.height / 2.8,
              color: CustomColors.lightGreen,
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: SvgPicture.asset("assets/reversedLeaf.svg"),
          ),
          Positioned(
            height: MediaQuery.of(context).size.height / 2.8,
            // width: double.infinity,
            top: 0,
            left: 0,
            right: 0,
            child: SafeArea(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                height: MediaQuery.of(context).size.height / 2.8,
                child: buildTopRow(),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 3.6,
            // top: 40,
            left: 30,
            right: 30,
            height: 100,
            child: buildMiddleContainer(),
          ),
        ],
      ),
    );
  }

  Row buildTopRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Good Morning",
              style: TextStyle(
                  fontFamily: "Futura",
                  fontSize: 16,
                  color: CustomColors.white.withOpacity(0.55)),
            ),
            buildUsernameAndBadge(),
            SizedBox(
              height: 5,
            ),
            buildBadgeAndPoints(),
          ],
        ),
        Spacer(),
        SvgPicture.asset("assets/whiteTreeIcon.svg")
      ],
    );
  }

  Row buildBadgeAndPoints() {
    return Row(
      children: [
        SvgPicture.asset("assets/smallBadge.svg"),
        SizedBox(width: 5),
        Text("10,234",
            style: TextStyle(
              fontFamily: "Futura",
              fontSize: 14,
              color: CustomColors.vLightGreen,
            )),
      ],
    );
  }

  Widget buildUsernameAndBadge() {
    return Row(
      children: [
        Text(
          "Mehdi",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
              fontFamily: "Futura", fontSize: 22, color: CustomColors.white),
        ),
        SizedBox(
          width: 10,
        ),
        SvgPicture.asset("assets/badge.svg"),
        // Spacer(),
        // SvgPicture.asset(
        //   "assets/whiteTreeIcon.svg",
        // )
      ],
    );
  }

  Container buildMiddleContainer() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      padding: EdgeInsets.all(20).copyWith(right: 10),
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset("assets/forest.svg"),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Tree Inventory",
                  style: TextStyle(
                    fontFamily: "JosefinSans",
                    fontSize: 18,
                  ),
                ),
                Text(
                  "Explore & Report trees all over the country",
                  style: TextStyle(
                      fontFamily: "Futura",
                      fontSize: 11,
                      color: CustomColors.lightGrey),
                ),
              ],
            ),
          ),
          Icon(
            Icons.arrow_forward_ios_sharp,
            size: 13,
            color: CustomColors.lightGrey,
          )
        ],
      ),
    );
  }
}
