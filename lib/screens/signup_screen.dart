import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:harabharapakistan/helper/colors.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();

  String routeName = "/signUpScreen";
}

class _SignupScreenState extends State<SignupScreen> {
  bool passwordHidden = true;
  bool agreementAccepted = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus.unfocus();
          }
        },
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Positioned(
                right: -20,

                // alignment: Alignment.topRight,
                // heightFactor: ,
                child: SvgPicture.asset(
                  "assets/leaf.svg",
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        "assets/justTree.svg",
                        alignment: Alignment.centerLeft,
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Expanded(
                        child: ListView(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Create A New Account",
                              style: TextStyle(
                                fontFamily: "JosefinSans",
                                fontSize: 22,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Full Name*",
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.green,
                              ),
                            ),
                            buildFullNameTextField(),
                            Text(
                              "Mobile Number*",
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.green,
                              ),
                            ),
                            buildMobileNumberTextField(),
                            Row(
                              children: [
                                Text(
                                  "Password*",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: CustomColors.green,
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  "Password should contain letters and digits",
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: CustomColors.lightGreen,
                                  ),
                                ),
                              ],
                            ),
                            buildPasswordTextField(),
                            Text(
                              "Confirm Password*",
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.green,
                              ),
                            ),
                            buildConfirmPasswordTextField(),
                            Text(
                              "Email Address",
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.green,
                              ),
                            ),
                            buildFullEmailTextField(),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Checkbox(
                                  // hoverColor: CustomColors.lightGreen,

                                  focusColor: CustomColors.lightGreen,
                                  checkColor: CustomColors.white1,
                                  value: agreementAccepted,
                                  onChanged: (value) {
                                    setState(() {
                                      agreementAccepted = value;
                                    });
                                  },
                                ),
                                Text(
                                  "I agree to the terms and conditions",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: CustomColors.grey,
                                    fontFamily: "Futura",
                                  ),
                                ),
                              ],
                            ),
                            buildSignUpButton(),
                            SizedBox(
                              height: 20,
                            ),
                            buildBottomMost(),

                            // Spacer(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Row buildBottomMost() {
    return Row(
      children: [
        Spacer(),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Already have and Account?",
              style: TextStyle(
                fontFamily: "Futura",
                fontSize: 16,
                color: CustomColors.grey,
              ),
            ),
            Text(
              "Sign In Here",
              style: TextStyle(
                fontFamily: "Futura",
                fontSize: 16,
                color: CustomColors.green,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Spacer(),
      ],
    );
  }

  SizedBox buildSignUpButton() {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        textColor: Colors.white,
        color: CustomColors.lightGreen,
        child: Text(
          "Sign Up",
          style: TextStyle(fontFamily: "Futura", fontSize: 18),
        ),
        onPressed: () {},
      ),
    );
  }

  GestureDetector buildForgotPassword() {
    return GestureDetector(
      onTap: () {
        //forgot password
      },
      child: Row(
        children: [
          Spacer(),
          Text(
            "Forgot Password?",
            style: TextStyle(
              fontFamily: "Futura",
              fontSize: 16,
              color: CustomColors.green,
            ),
          ),
        ],
      ),
    );
  }

  Container buildMobileNumberTextField() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(left: 0),
            icon: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: 10,
                ),
                Text(
                  "+92",
                  style: TextStyle(),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  width: 0.5,
                  height: 40,
                  // height: double.maxFinite,
                  color: CustomColors.darkGreen,
                )
              ],
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'xxx xxxxxxx',
          ),
        ),
      ),
    );
  }

  Container buildPasswordTextField() {
    return Container(
      decoration: BoxDecoration(
        // border: Border.all(
        //     // color: CustomColors.white1,
        //     ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          obscureText: passwordHidden,
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    passwordHidden = !passwordHidden;
                  });
                },
                child: Icon(
                    passwordHidden ? Icons.visibility_off : Icons.visibility)),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'Type your password here',
          ),
        ),
      ),
    );
  }

  Container buildConfirmPasswordTextField() {
    return Container(
      decoration: BoxDecoration(
        // border: Border.all(
        //     // color: CustomColors.white1,
        //     ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          obscureText: passwordHidden,
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    passwordHidden = !passwordHidden;
                  });
                },
                child: Icon(
                    passwordHidden ? Icons.visibility_off : Icons.visibility)),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'Type your password here',
          ),
        ),
      ),
    );
  }

  Container buildFullNameTextField() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          // keyboardType: TextInputType.number,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'Type Here',
          ),
        ),
      ),
    );
  }

  Container buildFullEmailTextField() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 10),
      height: 60,
      child: Center(
        child: TextFormField(
          // keyboardType: TextInputType.number,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'Type Here',
          ),
        ),
      ),
    );
  }
}
