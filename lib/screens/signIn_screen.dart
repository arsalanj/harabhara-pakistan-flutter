import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:harabharapakistan/helper/colors.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();

  String routeName = "/signInScreen";
}

class _SignInScreenState extends State<SignInScreen> {
  bool passwordHidden = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus.unfocus();
          }
        },
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Positioned(
                right: -20,

                // alignment: Alignment.topRight,
                // heightFactor: ,
                child: SvgPicture.asset(
                  "assets/leaf.svg",
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SvgPicture.asset("assets/justTree.svg"),
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        "Sign In To Your Account",
                        style: TextStyle(
                          fontFamily: "JosefinSans",
                          fontSize: 22,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Mobile Number",
                        style: TextStyle(
                          fontSize: 18,
                          color: CustomColors.green,
                        ),
                      ),
                      buildMobileTextField(),
                      Text(
                        "Password",
                        style: TextStyle(
                          fontSize: 18,
                          color: CustomColors.green,
                        ),
                      ),
                      buildPasswordTextField(),
                      buildForgotPassword(),
                      Spacer(),
                      buildLoginButton(),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Spacer(),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "Don't have and Account?",
                                style: TextStyle(
                                  fontFamily: "Futura",
                                  fontSize: 16,
                                  color: CustomColors.grey,
                                ),
                              ),
                              Text(
                                "Sign Up Here",
                                style: TextStyle(
                                  fontFamily: "Futura",
                                  fontSize: 16,
                                  color: CustomColors.green,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),

                      // Spacer(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  SizedBox buildLoginButton() {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        textColor: Colors.white,
        color: CustomColors.lightGreen,
        child: Text(
          "Login",
          style: TextStyle(fontFamily: "Futura", fontSize: 18),
        ),
        onPressed: () {},
      ),
    );
  }

  GestureDetector buildForgotPassword() {
    return GestureDetector(
      onTap: () {
        //forgot password
      },
      child: Row(
        children: [
          Spacer(),
          Text(
            "Forgot Password?",
            style: TextStyle(
              fontFamily: "Futura",
              fontSize: 16,
              color: CustomColors.green,
            ),
          ),
        ],
      ),
    );
  }

  Container buildPasswordTextField() {
    return Container(
      decoration: BoxDecoration(
        // border: Border.all(
        //     // color: CustomColors.white1,
        //     ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          obscureText: passwordHidden,
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    passwordHidden = !passwordHidden;
                  });
                },
                child: Icon(
                    passwordHidden ? Icons.visibility_off : Icons.visibility)),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'Type your password here',
          ),
        ),
      ),
    );
  }

  Container buildMobileTextField() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: CustomColors.white1,
      ),
      margin: EdgeInsets.only(top: 10, bottom: 20),
      height: 60,
      child: Center(
        child: TextFormField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(left: 0),
            icon: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: 10,
                ),
                Text(
                  "+92",
                  style: TextStyle(),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  width: 0.5,
                  height: 40,
                  // height: double.maxFinite,
                  color: CustomColors.darkGreen,
                )
              ],
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            ),
            hintText: 'xxx xxxxxxx',
          ),
        ),
      ),
    );
  }
}
